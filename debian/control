Source: ros2-rcl-interfaces
Section: devel
Priority: optional
Maintainer: Debian Robotics Team <team+robotics@tracker.debian.org>
Uploaders: Timo Röhling <roehling@debian.org>, 
	   Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends:
 debhelper-compat (= 13),
 dh-ros,
 ament-cmake,
 ament-lint,
 ament-cmake-googletest,
 cmake,
 rosidl-cmake,
 ros2-unique-identifier-msgs,
 ros2-rosidl-core-generators,
 ros2-rosidl-default-generators,
 ros2-test-interface-files,
Homepage: https://github.com/ros2/rcl_interfaces
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/robotics-team/ros2-rcl-interface.git
Vcs-Browser: https://salsa.debian.org/robotics-team/ros2-rcl-interfaces
Description: ROS 2 set of packages that contain interface files (.msg and .srv) 
 This package contains a set of packages that primarily contain interface 
 files (.msg and .srv) which are used both to implement client library 
 concepts and for testing.

Package: ros2-action-msgs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 several messages and services useful for ROS 2 actions.
 ${source:Extended-Description}

Package: ros2-builtin-interfaces
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message and service definitions for OMG IDL Platform Specific Model.
 ${source:Extended-Description}

Package: ros2-composition-interfaces
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message and service definitions for managing composable nodes.
 ${source:Extended-Description}

Package: ros2-lifecycle-msgs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message and service definitions for managing lifecycle nodes.
 This package contains message and service definitions for managing lifecycle 
 nodes. These messages and services form a standardized interface for 
 transitioning these managed nodes through a known state-machine.
 .
 ${source:Extended-Description}

Package: ros2-rcl-interfaces
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 messages and services to communicate higher level concepts
 This package contains the messages and services which ROS client libraries 
 will use under the hood to communicate higher level concepts such as 
 parameters.
 .
 ${source:Extended-Description}

Package: ros2-rosgraph-msgs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 messages definitions relating ROS Computation Graph
 This is a package containing message definitions relating to the ROS 
 Computation Graph. These are generally considered to be low-level 
 messages that end users do not interact with.
 .
 ${source:Extended-Description}

Package: ros2-service-msgs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message types used by ROS services
 This package contains message types used by ROS services.
 .
 ${source:Extended-Description}

Package: ros2-statistics-msgs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message definitions for reporting statistics
 This package contains ROS 2 message definitions for reporting statistics 
 for topics and system resources.
 .
 ${source:Extended-Description}

#Package: ros2-test-msgs
#Architecture: all
#Multi-Arch: foreign
#Depends:
# ${misc:Depends},
#Description: ROS2 message definitions and fixtures exclusively for testing purposes
# This package contains ROS 2 message definitions and fixtures used 
# exclusively for testing purposes
# .
# ${source:Extended-Description}

Package: ros2-type-description-interfaces
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: ROS2 message and service definitions of other types
 This package contains ROS 2 message and service definitions for describing 
 and communicating descriptions of other types.
 .
 ${source:Extended-Description}
